// ==UserScript==
// @name         Ronis Projects Helper
// @namespace    https://projects.ronis.info/
// @version      0.1.4
// @description  try to take over the world!
// @author       You
// @match        https://projects.ronis.info/view/*
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    var btn = document.querySelector("#schedule > a.btn"),
        activityElements,
        settings = JSON.parse(window.localStorage.getItem("vscustom")) || {
            'Code review': {
                comment: 'Code review',
                assignees: {
                    '178': 'Николай',
                    '131': 'Артем'
                }
            }
        },
        i,
        el,
        markdown;
        console.log(settings);
    if (btn) {
        var btnWrapper = document.createElement('div');
        btn.style.backgroundColor = 'lightgrey';
        btnWrapper.classList.add("btn-group");
        btnWrapper.appendChild(btn);
        if ($) {
            var $dropdownMenuButton = $('<button data-toggle="dropdown" class="btn btn-sm dropdown-toggle btn-default" style="background-color: lightgrey"><i class="caret"></i></button>'),
                $dropdownMenuList = $('<ul class="dropdown-menu" style="background-color: lightgrey"></ul>'),
                first = true,
                createAssignButton = function (innerContent, key, action) {
                    var assignToEl = btn.cloneNode(true),
                        listEl = document.createElement('li');
                    assignToEl.innerHTML = innerContent;
                    assignToEl.classList.add('vs-custom-button');
                    listEl.appendChild(assignToEl);
                    $dropdownMenuList.append($(listEl));
                    assignToEl.setAttribute('assignee_id', key);
                    if (action) {
                        assignToEl.setAttribute('action', action);
                    }
                };
            $(btnWrapper).append($dropdownMenuButton);
            $(btnWrapper).append($dropdownMenuList);

            if (settings) {
                Object.keys(settings).forEach(function (action) {
                    if (!first) {
                        $dropdownMenuList.append('<li class="divider"></li>');
                    }
                    first = false;
                    if (settings[action].assignees) {
                        Object.keys(settings[action].assignees).forEach(function (item) {
                            createAssignButton(action + ' to ' + settings[action].assignees[item], item, action);
                        });
                    }
                });
            }

            $dropdownMenuList.append('<li class="divider"></li>');

            $('.sidebar-right a[data-user-id]').each(function () {
                createAssignButton('Add schedule for ' + $(this).text(), $(this).data('user-id'));
            });

            $dropdownMenuList.append('<li class="divider"></li>');
            createAssignButton('Settings', '', 'settings');

            $dropdownMenuList.on("click", ".vs-custom-button", function () {
                var self = this,
                    tSettings;
                if ($(self).attr('action') === 'settings') {
                    while (!tSettings) {
                        try {
                            tSettings = prompt("Please enter settings (JSON string):");
                            if (!tSettings) {
                                break;
                            }
                            settings = JSON.parse(tSettings);
                            localStorage.setItem("vscustom", tSettings);
                            window.location.reload();
                        } catch (e) {
                            alert(e);
                            tSettings = false;
                        }
                    }
                    return false;
                }
                $(document).one('show.bs.modal', function () {
                    var assigneeId = $(self).attr('assignee_id'),
                        $assigneeField = $(this).find('[name=assignee_id]'),
                        $description = $(this).find('[name=description]'),
                        action = $(self).attr('action');
                    $assigneeField.val(assigneeId);
                    $assigneeField.selectpicker('refresh');
                    if (action && settings[action].comment) {
                        $description.val(settings[action].comment);
                    }
                });
            });
        }
        var topControl = document.querySelector('.top-control');
        if (topControl) {
            topControl.insertBefore(btnWrapper, topControl.firstChild);
        }
    }

    activityElements = document.getElementById("all").querySelectorAll(".media.activity.activity-history");
    for (i = 0; i < activityElements.length; i += 1) {
        el = activityElements[i];
        markdown = el.querySelector(".markdown");

        if (markdown && markdown.innerHTML === '<p>Changed description</p>') {
            el.classList.add("hidden");
        }
    }
})();
